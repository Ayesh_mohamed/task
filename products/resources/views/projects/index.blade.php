 @extends('layouts.main')

 @section('content')
    <a href="{{route('projects.create')}}">Create Project</a>

    <table id="project_table">
        <thead>
            <th>id</th>
            <th>title</th>
            <th>done percentage</th>
            <th>actions</th>
        </thead>

        <tbody>
            @foreach($projects as $project)
            <tr>
              <td>{{$project->id}}</td>
              <td>{{$project->title}}</td>  
              @php
                $done_tasks = $project->tasks()->where('status','done')->count();
              @endphp
              <td>{{ $done_tasks ? ($done_tasks / $project->tasks->count()) : 0}}</td>
              <td>
              <a href="{{route('projects.edit',['id'=>$project->id])}}">Update Project</a>

              <form action="{{route('projects.destroy',['id'=>$project->id])}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="Delete Project ">
              </form>

              </td>  
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection