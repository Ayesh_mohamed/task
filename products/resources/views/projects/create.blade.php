@extends('layouts.main')

@section('content')
    <form action="{{route('projects.store')}}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-6">
                <label for="title">Project title</label>
                <input type="text" class="form-control" name="title">
            </div>
            <div class="form-group col-6">
                <label for="description">Project description</label>
                <input type="text" id="description" class="form-control" name="description">
            </div>
        </div>
        <br>
        <hr>
        <br>
        <div class="row repeater">
            <div data-repeater-list="tasks">
                <div data-repeater-item>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="title">Task title</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        {{-- <div class="form-group col-6">
                            <label for="description">Task description</label>
                            <input type="text" id="description" class="form-control" name="description">
                        </div> --}}
                        <div class="form-group col-6">
                            <label for="description">Task Status</label>
                            <select name="status" id="">
                                <option value="done">Done</option>
                                <option value="not_done">Not Done</option>
                            </select>
                        </div>
                     <input data-repeater-delete type="button" value="Delete Task" class="col-3 mt-3"/>
                    </div>

                </div>
              </div>
              <input data-repeater-create type="button" value="Add Task" class="mt-3 col-3" />
        </div>

        <input type="submit" value="Submit">
    </form>
@endsection