<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();

        return view('projects.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validateCreate($request->all());
            // dd($request->all());    
        if(!$validate->fails()){
            $project = Project::create($request->except('tasks'));
            
            // Create Project tasks
            foreach ($request->tasks as $task) {
                Task::create([
                    'project_id' => $project->id,
                    'title' => $task['title'],
                    'status' => $task['status'] ? $task['status'] : null,
                ]);
            }
        }

        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);

        return view('projects.show',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::where('id',$id)->with('tasks')->first();

        return view('projects.update',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data= $request->all();
        $data['id'] = $id;
        $validate = $this->validateUpdate($data);

        if(!$validate->fails()){
            $project =Project::find($id);
             $project->update($request->except(['tasks','_token','_method']));
            
            // Create Project tasks
            Task::where('project_id',$id)->delete();
            foreach ($request->tasks as $task) {
                Task::create([
                    'project_id' => $project->id,
                    'title' => $task['title'],
                    'status' => $task['status'] ? $task['status'] : null,
                ]);
            }
        }
        return redirect()->route('projects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::where('id',$id)->delete();

        return redirect()->route('projects.index');
    }

    public function validateCreate($data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:191',
            'description' => 'nullable|string|max:6657',
            'tasks' => 'required|array',
            'tasks.*.title' => 'required|string|max:191',
            'tasks.*.status' => ['required',Rule::in(['done','not_done'])],
            'tasks.*.description' => 'nullable|string|max:6657',
        ]);
    }

    public function validateUpdate($data)
    {
        return Validator::make($data, [
            'id' => 'required|exists:projects,id',
            'title' => 'required|string|max:191',
            'description' => 'nullable|string|max:6657',
            'tasks' => 'required|array',
            'tasks.*.title' => 'required|string|max:191',
            'tasks.*.status' => ['required',Rule::in(['done','not_done'])],
            'tasks.*.description' => 'nullable|string|max:6657',
        ]);
    }
}
