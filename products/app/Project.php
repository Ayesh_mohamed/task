<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable =['id','title','description','created_at','updated_at'];

    protected $table="projects";

    public function tasks()
    {
        return $this->hasMany(Task::class,'project_id','id');
    }
}
