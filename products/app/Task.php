<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['id','project_id','title','description','status'];
    protected $table="tasks";

    public function project()
    {
        return $this->belongsTo(Project::class,'project_id','id');
    }
}
